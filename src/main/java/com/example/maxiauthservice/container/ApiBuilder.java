package com.example.maxiauthservice.container;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;

public interface ApiBuilder {

    Logger log = LoggerFactory.getLogger(ApiBuilder.class);


    default String timeStamp(){
        return LocalDateTime.now(ZoneId.of("UTC")).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    /**
     * Generates ApiError format depending on exception.
     *
     * @param ex
     * @return
     */
    default ApiError generateApiError(RuntimeException ex){
        log.info("Generating ApiError for error with code: {}", ex.getMessage());

        ApiError apiError = new ApiError();
        apiError.setCode(ex.getLocalizedMessage());
        apiError.setDescription(ex.getMessage());
        apiError.setReason(ex.getMessage());
        return apiError;
    }

    default ApiError generateApiError(MethodArgumentNotValidException ex){
        log.info("Generating ApiError for error with code: {}", ex.getMessage());

        ApiError apiError = new ApiError();
        apiError.setCode(ex.getBindingResult().getAllErrors().get(0).getCode());
        apiError.setDescription(ex.getBindingResult().getAllErrors().get(0).getDefaultMessage());
        apiError.setReason(ex.getBindingResult().getAllErrors().get(0).getDefaultMessage());
        return apiError;
    }

    default ApiInfo generateApiInfo(RuntimeException ex){
        log.info("Generating ApiInfo for status failed");

        ApiInfo apiInfo = new ApiInfo();
        apiInfo.setTimestamp(timeStamp());
        apiInfo.setSuccess(false);
        apiInfo.setStatus(ApiConstants.STATUS_FAILED);
        apiInfo.setErrors(Collections.singletonList(generateApiError(ex)));

        return apiInfo;
    }

    default ApiInfo generateApiInfo(MethodArgumentNotValidException ex){
        log.info("Generating ApiInfo for status failed");

        ApiInfo apiInfo = new ApiInfo();
        apiInfo.setTimestamp(timeStamp());
        apiInfo.setSuccess(false);
        apiInfo.setStatus(ApiConstants.STATUS_FAILED);
        apiInfo.setErrors(Collections.singletonList(generateApiError(ex)));

        return apiInfo;
    }

    default ApiInfo generateApiInfo(){
        log.info("Generating ApiInfo for status okay");

        ApiInfo apiInfo = new ApiInfo();
        apiInfo.setTimestamp(timeStamp());
        apiInfo.setSuccess(true);
        apiInfo.setStatus(ApiConstants.STATUS_OK);
        return apiInfo;
    }

    default ApiMessage generateApiMessage(){
        log.info("Generating ApiMessage");

        ApiMessage apiMessage = new ApiMessage();
        apiMessage.setInfo(generateApiInfo());
        return apiMessage;
    }

    default ApiMessage generateApiMessage(ApiInfo apiInfo){
        log.info("Generating ApiMessage");

        ApiMessage apiMessage = new ApiMessage();
        apiMessage.setInfo(apiInfo);
        return apiMessage;
    }


    default <L extends Serializable> SingleMessage<L> generateSingleMessage(L data){
        SingleMessage<L> singleMessage = new SingleMessage<L>();
        singleMessage.setItem(data);
        singleMessage.setInfo(generateApiInfo());
        return singleMessage;
    }

    default <L extends Serializable> CollectionMessage<L> generateCollectionMessage(Collection<L> collection){
        CollectionMessage<L> collectionMessage = new CollectionMessage<L>();
        collectionMessage.setItems(collection);
        collectionMessage.setInfo(generateApiInfo());
        return collectionMessage;
    }
}
