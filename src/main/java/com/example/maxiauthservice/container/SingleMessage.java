package com.example.maxiauthservice.container;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
public class SingleMessage<D extends Serializable> extends ApiMessage implements Serializable {
    private static final long serialVersionUID = -321813317065626137L;

    private D item;
}
