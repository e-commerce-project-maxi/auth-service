package com.example.maxiauthservice.container;

import lombok.Data;

import java.io.Serializable;

@Data
public class ApiError implements Serializable {

    private static final long serialVersionUID = -6129047534180761073L;

    private String code;
    private String reason;
    private String description;
}
