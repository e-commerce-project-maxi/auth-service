package com.example.maxiauthservice.container;

import lombok.Data;

import java.io.Serializable;

@Data
public class ApiMessage implements Serializable {
    private ApiInfo info;
}
