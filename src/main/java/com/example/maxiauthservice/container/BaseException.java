package com.example.maxiauthservice.container;

public interface BaseException {

    String getCode();

    String getDescription();
}
