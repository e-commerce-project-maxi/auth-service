package com.example.maxiauthservice.container;

import lombok.Data;

import java.io.Serializable;
import java.util.Collection;

@Data
public class ApiInfo implements Serializable {
    private static final long serialVersionUID = 7480592716876162744L;

    private boolean success;
    private String status;
    private String requestId;
    private String responseId;
    private String timestamp;
    private Collection<ApiError> errors;

}
