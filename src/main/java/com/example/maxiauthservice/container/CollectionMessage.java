package com.example.maxiauthservice.container;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Collection;

@Data
@EqualsAndHashCode(callSuper = false)
public class CollectionMessage<D extends Serializable> extends ApiMessage implements Serializable {
    private static final long serialVersionUID = -8674145381371582606L;

    private Collection<D> items;

}
