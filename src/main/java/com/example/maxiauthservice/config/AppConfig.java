package com.example.maxiauthservice.config;

import com.example.maxiauthservice.tk.model.JwtTokenParameter;
import com.example.maxiauthservice.tk.util.JwtTokenGenerator;
import com.example.maxiauthservice.tk.util.JwtTokenUtil;
import com.example.maxiauthservice.tk.util.JwtTokenValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.expiration}")
    private Long expiration;
    @Value("${jwt.prefix}")
    private String prefixToken;

    @Bean
    public JwtTokenParameter getJwtTokenParameter() {
        return new JwtTokenParameter(secret, expiration,prefixToken);
    }

    @Bean
    public JwtTokenUtil getJwtTokenUtil() {
        return new JwtTokenUtil(secret);
    }

    @Bean
    public JwtTokenGenerator getJwtTokenGenerator() {
        return new JwtTokenGenerator(getJwtTokenParameter());
    }

    @Bean
    public JwtTokenValidator getJwtTokenValidator() {
        return new JwtTokenValidator(getJwtTokenParameter());
    }
}
