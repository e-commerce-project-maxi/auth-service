package com.example.maxiauthservice.constant;

public interface Constants {
    long TOKEN_TTL = 600;
    int MAX_ATTEMPT = 3;
    String SUCCESS = "SUCCESS";
}
