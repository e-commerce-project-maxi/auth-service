package com.example.maxiauthservice.repository;

import com.example.maxiauthservice.model.jwt.JwtRedisModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JwtRedisRepository extends CrudRepository<JwtRedisModel, Long> {
     Optional<JwtRedisModel> findByUsername(String username);
}
