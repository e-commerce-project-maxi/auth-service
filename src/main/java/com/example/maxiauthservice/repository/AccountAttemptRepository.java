package com.example.maxiauthservice.repository;

import com.example.maxiauthservice.entity.AccountAttempt;
import com.example.maxiauthservice.model.accountAttempt.AccountStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountAttemptRepository extends JpaRepository<AccountAttempt, Long> {
   // String query = "select new com.example.maxiauthservice.model.accountAttempt.AccountStatus(a.status) from AccountAttempt a where a.username =?1";

    @Query("select new com.example.maxiauthservice.model.accountAttempt.AccountStatus(a.status) from AccountAttempt a where a.username =?1")
    AccountStatus getStatusByUsername(String username);

    AccountAttempt findByUsername(String username);
}
