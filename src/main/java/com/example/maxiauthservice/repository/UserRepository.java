package com.example.maxiauthservice.repository;

import com.example.maxiauthservice.entity.BackUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<BackUser,Long> {
    Optional<BackUser> findByUsername(String username);
}
