package com.example.maxiauthservice.validation.annotation.validator;

import com.example.maxiauthservice.enums.Status;
import com.example.maxiauthservice.service.AccountAttemptService;
import com.example.maxiauthservice.validation.annotation.AccountBlocked;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
@RequiredArgsConstructor
public class AccountBlockedValidator implements ConstraintValidator<AccountBlocked, String> {

    private final AccountAttemptService accountAttemptService;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return getStatusByUsername(value);
    }

    private boolean getStatusByUsername(String username) {
        Status status = accountAttemptService.getStatusFromAccountAttemptByUsername(username);
        return status.equals(Status.ACTIVE);
    }
}
