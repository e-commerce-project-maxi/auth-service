package com.example.maxiauthservice.validation.annotation;


import com.example.maxiauthservice.validation.annotation.validator.AccountBlockedValidator;
import com.example.maxiauthservice.validation.annotation.validator.BirthDateValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;

@Target({FIELD, PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = AccountBlockedValidator.class)
public @interface AccountBlocked {

    String message() default "Account Is Blocked.Please contact to administration";
    //represents group of constraints
    Class<?>[] groups() default {};
    //represents additional information about annotation
    Class<? extends Payload>[] payload() default {};
}
