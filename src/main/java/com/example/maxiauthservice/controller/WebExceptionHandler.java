package com.example.maxiauthservice.controller;


import com.example.maxiauthservice.container.ApiBuilder;
import com.example.maxiauthservice.container.ApiMessage;
import com.example.maxiauthservice.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@Slf4j
@RestControllerAdvice
public class WebExceptionHandler implements ApiBuilder {

    @ExceptionHandler(NotFoundException.class)
    protected ResponseEntity<ApiMessage> handleConflict(NotFoundException ex, WebRequest request) {
        log.error("Exception occurred, ", ex);
        ApiMessage apiMessage = generateApiMessage(generateApiInfo(ex));
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .contentType(MediaType.APPLICATION_JSON)
                .body(apiMessage);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiMessage> handleConflict(final MethodArgumentNotValidException ex) {
        log.error("Exception occurred, ", ex);
        ApiMessage apiMessage = generateApiMessage(generateApiInfo(ex));
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(apiMessage);
    }

    @ExceptionHandler(RuntimeException.class)
    protected ResponseEntity<ApiMessage> handleConflict(RuntimeException ex, WebRequest request) {
        log.error("Exception occurred, ", ex);
        ApiMessage apiMessage = generateApiMessage(generateApiInfo(ex));
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .contentType(MediaType.APPLICATION_JSON)
                .body(apiMessage);
    }
}
