package com.example.maxiauthservice.controller;

import com.example.maxiauthservice.container.ApiBuilder;
import com.example.maxiauthservice.container.CrossSafeRestResource;
import com.example.maxiauthservice.container.SingleMessage;
import com.example.maxiauthservice.dto.email.EmailRequest;
import com.example.maxiauthservice.dto.payload.ChangePasswordRequest;
import com.example.maxiauthservice.dto.payload.JwtTokenResponse;
import com.example.maxiauthservice.dto.payload.LoginRequest;
import com.example.maxiauthservice.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.Valid;

@RequiredArgsConstructor
@CrossSafeRestResource(path = "/auth")
public class AuthController implements ApiBuilder {

    private final AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<SingleMessage<JwtTokenResponse>> logIn(
            @RequestBody @Valid LoginRequest loginRequest) {
        return ResponseEntity.ok(generateSingleMessage(authService.login(loginRequest)));
    }

    @PostMapping("/forgot-password")
    public ResponseEntity<SingleMessage<String>> forgotPassword(
            @RequestBody @Valid EmailRequest email) {
        return ResponseEntity.ok(generateSingleMessage(authService.forgotPassword(email)));
    }

    @PostMapping("/change-password")
    public ResponseEntity<SingleMessage<String>> changePassword(
            @RequestBody @Valid ChangePasswordRequest changePasswordRequest) {
        return ResponseEntity.ok(generateSingleMessage(authService.changePassword(changePasswordRequest)));
    }
}
