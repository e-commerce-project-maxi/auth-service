package com.example.maxiauthservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaxiAuthServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MaxiAuthServiceApplication.class, args);
    }

}
