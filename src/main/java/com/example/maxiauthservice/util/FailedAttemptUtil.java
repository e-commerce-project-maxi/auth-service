package com.example.maxiauthservice.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class FailedAttemptUtil {

    public Integer updateFailedAttempt(Integer attempt) {
        return Math.addExact(attempt, 1);
    }
}
