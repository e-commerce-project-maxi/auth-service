package com.example.maxiauthservice.tk.util;

import com.example.maxiauthservice.tk.model.JwtTokenParameter;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;
import java.util.Objects;


public class JwtTokenValidator {
    private final JwtTokenParameter jwtTokenParameter;
    private final JwtTokenUtil jwtTokenUtil;

    public JwtTokenValidator(JwtTokenParameter jwtTokenParameter) {
        this.jwtTokenParameter = jwtTokenParameter;
        this.jwtTokenUtil = new JwtTokenUtil();
    }

    public boolean validateToken(String token, UserDetails userDetails) {
        String username = this.jwtTokenUtil.getUsernameFromToken(token);
        return Objects.equals(username, userDetails.getUsername()) && !this.isTokenExpired(token);
    }

    public boolean validateToken(String token) {
        return this.isTokenExpired(token);
    }

    public boolean canTokenBeRefreshed(String token) {
        return !this.isTokenExpired(token);
    }

    private boolean isTokenExpired(String token) {
        return this.jwtTokenUtil.getExpirationDateFromToken(token).before(new Date());
    }
}
