package com.example.maxiauthservice.tk.util;

import com.example.maxiauthservice.tk.model.JwtTokenParameter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;

import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class JwtTokenGenerator {
    private final JwtTokenParameter jwtTokenParameter;
    private final JwtTokenUtil jwtTokenUtil;

    public JwtTokenGenerator(JwtTokenParameter jwtTokenParameter) {
        this.jwtTokenParameter = jwtTokenParameter;
        this.jwtTokenUtil = new JwtTokenUtil(jwtTokenParameter.getSecret());
    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return this.doGenerationToken(claims, userDetails.getUsername());
    }

    public String refreshToken(String token) {
        Date issuedDate = new Date();
        Date expirationDate = new Date(System.currentTimeMillis() + this.jwtTokenParameter.getExpirationTime());
        Claims claims = this.jwtTokenUtil.getAllClaimsFromToken(token);
        claims.setIssuedAt(issuedDate);
        claims.setExpiration(expirationDate);
        token = Jwts.builder()
                .setClaims(claims)
                // .signWith(getSignInKey(), SignatureAlgorithm.HS512)
                .signWith(SignatureAlgorithm.HS512, jwtTokenParameter.getSecret())
                .compact();
        return jwtTokenParameter.getPrefixToken() + token;
    }

    public String doGenerationToken(Map<String, Object> claims, String subject) {
        Date issuedDate = new Date();
        Date expirationDate = new Date(System.currentTimeMillis() + this.jwtTokenParameter.getExpirationTime());
        log.info("Token issued at: {}", issuedDate);
        log.info("Expiration Date for token: {}", expirationDate);
        String token = Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(issuedDate)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, jwtTokenParameter.getSecret())
                .compact();
        return jwtTokenParameter.getPrefixToken() + token;
    }

    private SecretKey getSignInKey() {
        byte[] keyBytes = this.jwtTokenParameter.getSecret().getBytes(StandardCharsets.UTF_8);
        return Keys.hmacShaKeyFor(keyBytes);
    }
}