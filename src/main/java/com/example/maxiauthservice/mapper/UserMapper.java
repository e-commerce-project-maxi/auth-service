package com.example.maxiauthservice.mapper;


import com.example.maxiauthservice.dto.user.UserResponse;
import com.example.maxiauthservice.entity.BackUser;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserResponse toResponse(BackUser backUser);
}
