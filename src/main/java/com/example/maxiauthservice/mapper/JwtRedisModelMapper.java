package com.example.maxiauthservice.mapper;

import com.example.maxiauthservice.model.jwt.JwtRedisModel;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import static com.example.maxiauthservice.constant.Constants.TOKEN_TTL;


@Component
public class JwtRedisModelMapper {

    public JwtRedisModel createInstanceForUser(String token, JwtRedisModel jwtRedisModel, String username) {
        if (ObjectUtils.isEmpty(jwtRedisModel)) {
            jwtRedisModel = new JwtRedisModel();
        }
        jwtRedisModel.setToken(token);
        jwtRedisModel.setUsername(username);
        jwtRedisModel.setTtl(TOKEN_TTL);
        return jwtRedisModel;
    }
}