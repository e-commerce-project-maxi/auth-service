package com.example.maxiauthservice.enums;

import lombok.Getter;

public enum Status {
    ACTIVE(1,"ACTIVE"),BLOCKED(2,"BLOCKED");

    @Getter
    private int id;
    @Getter
    private String value;

    Status(int id, String value){
        this.id=id;
        this.value=value;
    }

    public static Status forNumber(Integer value) {
        switch (value) {
            case 1:
                return ACTIVE;
            case 2:
                return BLOCKED;
            default:
                return null;
        }
    }
}
