package com.example.maxiauthservice.enums;

public enum UserStatus {
    ACTIVE(1),
    IN_ACTIVE(2),
    DELETED(3);

    UserStatus(int id) {
        this.id = id;
    }

    private int id;

    public UserStatus getStatus(int id) {
        switch (id) {
            case 1:
                return UserStatus.ACTIVE;
            case 2:
                return UserStatus.IN_ACTIVE;
            default:
                return UserStatus.DELETED;
        }
    }

    public boolean checkStatus(int id) {
        switch (id) {
            case 1:
            case 2:
            case 3:
                return true;
            default:
                return false;
        }
    }

    public int getId() {
        return id;
    }
}
