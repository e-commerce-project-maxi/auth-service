package com.example.maxiauthservice.enums;

import org.springframework.http.HttpStatus;

public enum BusinessExceptionEnum {

    USER_NOT_FOUND("User not found by id: %s",
            HttpStatus.NOT_FOUND,
            "Error happened while getting user",
            "Error happened while getting user"),
    USER_NOT_FOUND_BY_EMAIL("User not found by email: %s",
            HttpStatus.NOT_FOUND,
            "Error happened while getting user by email",
            "Error happened while getting user by email"),
    USER_NOT_FOUND_BY_USERNAME("User not found by username: %s",
            HttpStatus.NOT_FOUND,
            "Error happened while getting user by username",
            "Error happened while getting user by username"),
    USERNAME_OR_PASSWORD_NOT_MATCH("Username or Password not matched",
            HttpStatus.BAD_REQUEST,
            "Username or Password not matched",
            "Username or Password not matched"),
    PASSWORD_EXPIRED_TIME("Password expired",
            HttpStatus.BAD_REQUEST,
            "Password need to change",
            "Password need to change"),
    PASSWORD_NOT_MATCH("Password not matched",
            HttpStatus.BAD_REQUEST,
            "Password not matched",
            "Password not matched");

    private final String message;
    private final HttpStatus code;
    private final String reason;
    private final String description;

    BusinessExceptionEnum(String message, HttpStatus code, String reason, String description) {
        this.message = message;
        this.code = code;
        this.reason = reason;
        this.description = description;
    }

    public String getMessage(Object... params) {
        return String.format(this.message, params);
    }

    public String getCode() {
        return this.code.toString();
    }

    public String getReason(Object... params) {
        return String.format(this.reason, params);
    }

    public String getDescription(Object... params) {
        return String.format(this.description, params);
    }
}
