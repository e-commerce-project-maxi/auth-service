package com.example.maxiauthservice.service;

import com.example.maxiauthservice.enums.Status;

public interface AccountAttemptService {

    Status getStatusFromAccountAttemptByUsername(String username);

    void updateFailedAttempt(String username);
}
