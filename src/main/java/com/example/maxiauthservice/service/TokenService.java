package com.example.maxiauthservice.service;

import org.springframework.security.core.userdetails.UserDetails;

public interface TokenService {

     String generateToken(UserDetails userDetails);

     String refreshToken(String token);

     boolean validateToken(String token, UserDetails userDetails);

     boolean canTokenBeRefreshed(String token);

     String getUsernameFromToken(String token);
}
