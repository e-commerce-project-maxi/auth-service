package com.example.maxiauthservice.service;


import com.example.maxiauthservice.dto.payload.ChangePasswordRequest;
import com.example.maxiauthservice.dto.user.UserResponse;

public interface UserService {

    UserResponse findUserByUsername(String username);

    void changePassword(ChangePasswordRequest changePasswordRequest);
}
