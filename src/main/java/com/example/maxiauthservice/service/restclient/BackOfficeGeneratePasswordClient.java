package com.example.maxiauthservice.service.restclient;

import com.example.maxiauthservice.container.SingleMessage;
import com.example.maxiauthservice.dto.email.EmailRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


@Slf4j
@Service
@RequiredArgsConstructor
public class BackOfficeGeneratePasswordClient {

    private final RestTemplate restTemplate;

    @Value("${back.office.password.url}")
    private String url;

    public String getGeneratedPasswordByEmail(EmailRequest email) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<EmailRequest> entity = new HttpEntity<>(email,headers);

        HttpEntity<SingleMessage> response = restTemplate.postForEntity(
                url,
                entity,
                SingleMessage.class);
        return Objects.requireNonNull(response.getBody()).getItem().toString();
    }
}
