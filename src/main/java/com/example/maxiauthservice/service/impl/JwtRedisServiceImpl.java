package com.example.maxiauthservice.service.impl;


import com.example.maxiauthservice.mapper.JwtRedisModelMapper;
import com.example.maxiauthservice.model.jwt.JwtRedisModel;
import com.example.maxiauthservice.repository.JwtRedisRepository;
import com.example.maxiauthservice.service.JwtRedisService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JwtRedisServiceImpl implements JwtRedisService {

    private final JwtRedisRepository jwtRedisRepository;
    private final JwtRedisModelMapper jwtRedisModelMapper;

    @Override
    public void createInstanceForUser(String token,String username) {
        JwtRedisModel jwtRedisModel = jwtRedisRepository.findByUsername(username).orElse(null);
        jwtRedisModel = jwtRedisModelMapper.createInstanceForUser(token, jwtRedisModel, username);
        jwtRedisRepository.save(jwtRedisModel);
    }
}
