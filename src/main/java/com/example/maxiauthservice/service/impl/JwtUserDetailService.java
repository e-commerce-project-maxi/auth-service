package com.example.maxiauthservice.service.impl;

import com.example.maxiauthservice.exception.UsernamePasswordException;
import com.example.maxiauthservice.model.jwt.JwtUser;
import com.example.maxiauthservice.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.example.maxiauthservice.enums.BusinessExceptionEnum.USERNAME_OR_PASSWORD_NOT_MATCH;


@Slf4j
@Service
@RequiredArgsConstructor
public class JwtUserDetailService implements UserDetailsService {

    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return Optional.of(userService.findUserByUsername(username))
                .map(JwtUser::new)
                .orElseThrow(RuntimeException::new);
    }
}
