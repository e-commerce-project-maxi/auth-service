package com.example.maxiauthservice.service.impl;

import com.example.maxiauthservice.dto.payload.ChangePasswordRequest;
import com.example.maxiauthservice.dto.user.UserResponse;
import com.example.maxiauthservice.entity.BackUser;
import com.example.maxiauthservice.enums.BusinessExceptionEnum;
import com.example.maxiauthservice.exception.PasswordException;
import com.example.maxiauthservice.exception.PasswordExpiredTimeException;
import com.example.maxiauthservice.mapper.UserMapper;
import com.example.maxiauthservice.repository.UserRepository;
import com.example.maxiauthservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

import static com.example.maxiauthservice.enums.BusinessExceptionEnum.PASSWORD_NOT_MATCH;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;

    @Override
    public UserResponse findUserByUsername(String username) {
        Optional<BackUser> userOptional = userRepository.findByUsername(username);
        userOptional.ifPresent(backUser -> checkPasswordExpiredTime(backUser.getPasswordExpiredTime()));
        return userOptional.map(userMapper::toResponse).orElse(null);
    }

    @Override
    public void changePassword(ChangePasswordRequest changePasswordRequest) {
        Optional<BackUser> userOptional = userRepository.findByUsername(changePasswordRequest.getUsername());
        if (userOptional.isPresent()) {
            BackUser backUser = userOptional.get();
            backUser.setPassword(passwordEncoder.encode(changePasswordRequest.getNewPassword()));
            userRepository.save(backUser);
        }
    }

    private void checkPasswordExpiredTime(LocalDateTime passwordChangeTime) {
        if (LocalDateTime.now().isAfter(passwordChangeTime)) {
            throw new PasswordExpiredTimeException(BusinessExceptionEnum.PASSWORD_EXPIRED_TIME);
        }
    }
}
