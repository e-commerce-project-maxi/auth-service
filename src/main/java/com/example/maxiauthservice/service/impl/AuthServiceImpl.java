package com.example.maxiauthservice.service.impl;

import com.example.maxiauthservice.dto.email.EmailRequest;
import com.example.maxiauthservice.dto.payload.ChangePasswordRequest;
import com.example.maxiauthservice.dto.payload.JwtTokenResponse;
import com.example.maxiauthservice.dto.payload.LoginRequest;
import com.example.maxiauthservice.exception.PasswordExpiredTimeException;
import com.example.maxiauthservice.exception.UsernamePasswordException;
import com.example.maxiauthservice.service.AccountAttemptService;
import com.example.maxiauthservice.service.AuthService;
import com.example.maxiauthservice.service.TokenService;
import com.example.maxiauthservice.service.UserService;
import com.example.maxiauthservice.service.restclient.BackOfficeGeneratePasswordClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import static com.example.maxiauthservice.constant.Constants.SUCCESS;
import static com.example.maxiauthservice.enums.BusinessExceptionEnum.PASSWORD_EXPIRED_TIME;
import static com.example.maxiauthservice.enums.BusinessExceptionEnum.USERNAME_OR_PASSWORD_NOT_MATCH;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
    private final AuthenticationManager authenticationManager;
    private final TokenService tokenService;
    private final BackOfficeGeneratePasswordClient backOfficeGeneratePasswordClient;
    private final UserService userService;
    private final AccountAttemptService accountAttemptService;

    @Override
    public JwtTokenResponse login(LoginRequest loginRequest) {
        UserDetails userDetails = getAuthentication(loginRequest.getUsername(), loginRequest.getPassword());
        String token = tokenService.generateToken(userDetails);
        return new JwtTokenResponse(token);
    }

    @Override
    public String forgotPassword(EmailRequest email) {
        return backOfficeGeneratePasswordClient.getGeneratedPasswordByEmail(email);
    }

    @Override
    public String changePassword(ChangePasswordRequest changePasswordRequest) {
        getAuthentication(changePasswordRequest.getUsername(), changePasswordRequest.getOldPassword());
        userService.changePassword(changePasswordRequest);
        return SUCCESS;
    }

    private UserDetails getAuthentication(String username, String password) {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            username,
                            password
                    )
            );
            return (UserDetails) authentication.getPrincipal();
        } catch (Exception e) {
            if (e.getMessage().contains("Password expired")) {
                throw new PasswordExpiredTimeException(PASSWORD_EXPIRED_TIME);
            }
            accountAttemptService.updateFailedAttempt(username);
            throw new UsernamePasswordException(USERNAME_OR_PASSWORD_NOT_MATCH);
        }
    }
}
