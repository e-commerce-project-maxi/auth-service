package com.example.maxiauthservice.service.impl;

import com.example.maxiauthservice.entity.AccountAttempt;
import com.example.maxiauthservice.enums.Status;
import com.example.maxiauthservice.repository.AccountAttemptRepository;
import com.example.maxiauthservice.service.AccountAttemptService;
import com.example.maxiauthservice.util.FailedAttemptUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

import static com.example.maxiauthservice.constant.Constants.MAX_ATTEMPT;

@Service
@RequiredArgsConstructor
public class AccountAttemptServiceImpl implements AccountAttemptService {

    private final AccountAttemptRepository accountAttemptRepository;

    @Override
    public Status getStatusFromAccountAttemptByUsername(String username) {
        return accountAttemptRepository.getStatusByUsername(username).getStatus();
    }

    @Override
    public void updateFailedAttempt(String username) {
        AccountAttempt accountAttempt = accountAttemptRepository.findByUsername(username);
        accountAttempt.setAttemptCount(FailedAttemptUtil.updateFailedAttempt(accountAttempt.getAttemptCount()));
        checkAttempt(accountAttempt);
        accountAttemptRepository.save(accountAttempt);
    }

    private void checkAttempt(AccountAttempt accountAttempt) {
        if (accountAttempt.getAttemptCount() == MAX_ATTEMPT) {
            accountAttempt.setStatus(Status.BLOCKED);
            accountAttempt.setModifiedTime(LocalDateTime.now());
        }
    }
}
