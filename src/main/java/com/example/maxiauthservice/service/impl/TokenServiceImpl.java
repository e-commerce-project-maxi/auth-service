package com.example.maxiauthservice.service.impl;

import com.example.maxiauthservice.service.TokenService;
import com.example.maxiauthservice.tk.util.JwtTokenGenerator;
import com.example.maxiauthservice.tk.util.JwtTokenUtil;
import com.example.maxiauthservice.tk.util.JwtTokenValidator;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
@RequiredArgsConstructor
public class TokenServiceImpl implements TokenService {

    private final JwtTokenGenerator jwtTokenGenerator;
    private final JwtTokenValidator jwtTokenValidator;
    private final JwtTokenUtil jwtTokenUtil;

    public String generateToken(UserDetails userDetails){
        return jwtTokenGenerator.generateToken(userDetails);
    }

    public String refreshToken(String token){
        return jwtTokenGenerator.refreshToken(token);
    }

    public boolean validateToken(String token, UserDetails userDetails){
        return jwtTokenValidator.validateToken(token, userDetails);
    }

    public boolean canTokenBeRefreshed(String token){
        return jwtTokenValidator.canTokenBeRefreshed(token);
    }

    public String getUsernameFromToken(String token){
        return jwtTokenUtil.getUsernameFromToken(token);
    }
}
