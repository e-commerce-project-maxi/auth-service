package com.example.maxiauthservice.service;

import com.example.maxiauthservice.dto.email.EmailRequest;
import com.example.maxiauthservice.dto.payload.ChangePasswordRequest;
import com.example.maxiauthservice.dto.payload.JwtTokenResponse;
import com.example.maxiauthservice.dto.payload.LoginRequest;

public interface AuthService {

    JwtTokenResponse login(LoginRequest loginRequest);

    String forgotPassword(EmailRequest email);

    String changePassword(ChangePasswordRequest changePasswordRequest);
}
