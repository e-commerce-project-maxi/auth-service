package com.example.maxiauthservice.dto.email;

import com.example.maxiauthservice.validation.annotation.Email;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EmailRequest {
    @Email
    private String email;
}
