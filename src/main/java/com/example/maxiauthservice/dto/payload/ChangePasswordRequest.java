package com.example.maxiauthservice.dto.payload;

import com.example.maxiauthservice.validation.annotation.AccountBlocked;
import com.example.maxiauthservice.validation.annotation.ValidPassword;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ChangePasswordRequest {
    @AccountBlocked
    String username;
    String oldPassword;
    @ValidPassword
    String newPassword;
}
