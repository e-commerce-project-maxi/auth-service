package com.example.maxiauthservice.dto.payload;

import com.example.maxiauthservice.validation.annotation.AccountBlocked;
import lombok.Data;

@Data
public class LoginRequest {
    @AccountBlocked
    private String username;
    private String password;
}
