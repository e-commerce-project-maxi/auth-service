package com.example.maxiauthservice.dto.user;

import lombok.Data;

@Data
public class PermissionResponse {
    private String name;
}
