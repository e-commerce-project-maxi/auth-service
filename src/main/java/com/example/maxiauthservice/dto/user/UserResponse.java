package com.example.maxiauthservice.dto.user;

import com.example.maxiauthservice.enums.Status;
import com.example.maxiauthservice.model.jwt.User;
import lombok.Data;

@Data
public class UserResponse implements User {
    private Long id;
    private String username;
    private String password;
    private Integer statusId;

    @Override
    public Status getStatus() {
        return null;
    }
}
