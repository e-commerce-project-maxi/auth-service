package com.example.maxiauthservice.dto.user;

import lombok.Data;

import java.util.List;

@Data
public class RoleResponse {
    private String name;
    private List<PermissionResponse> permissionList;
}
