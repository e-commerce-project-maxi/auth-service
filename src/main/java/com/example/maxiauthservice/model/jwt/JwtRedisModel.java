package com.example.maxiauthservice.model.jwt;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serializable;


@RedisHash(value = "Tokens")
@Data
public class JwtRedisModel implements Serializable {
    @Id
    private Long id;
    @Indexed
    private String username;
    private String token;
    private long ttl;
    //private Map<Integer, Short> menuJoin = new HashMap<>();
}
