package com.example.maxiauthservice.model.jwt;

import com.example.maxiauthservice.enums.Status;

public interface User {
    String getUsername();
    String getPassword();
    Status getStatus();
    Long getId();
}
