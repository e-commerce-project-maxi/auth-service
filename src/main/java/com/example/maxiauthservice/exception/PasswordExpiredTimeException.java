package com.example.maxiauthservice.exception;

import com.example.maxiauthservice.enums.ApiExceptionTypeEnum;
import com.example.maxiauthservice.enums.BusinessExceptionEnum;

public class PasswordExpiredTimeException extends BaseApiException {
    private static final long serialVersionUID = 7742409316646849742L;

    public PasswordExpiredTimeException(BusinessExceptionEnum exceptionEnum, Object... params) {
        super(exceptionEnum.getMessage(params),
                exceptionEnum.getCode(),
                exceptionEnum.getReason(params),
                exceptionEnum.getDescription(params));
    }

    public PasswordExpiredTimeException(String message, ApiExceptionTypeEnum apiExceptionTypeEnum) {
        super(message, apiExceptionTypeEnum);
    }
}
