package com.example.maxiauthservice.exception;


import com.example.maxiauthservice.enums.ApiExceptionTypeEnum;
import com.example.maxiauthservice.enums.BusinessExceptionEnum;

public class NotFoundException extends BaseApiException {
    private static final long serialVersionUID = 6009574796350506068L;

    public NotFoundException(BusinessExceptionEnum exceptionEnum, Object... params) {
        super(exceptionEnum.getMessage(params),
                exceptionEnum.getCode(),
                exceptionEnum.getReason(params),
                exceptionEnum.getDescription(params));
    }

    public NotFoundException(String message, ApiExceptionTypeEnum apiExceptionTypeEnum) {
        super(message, apiExceptionTypeEnum);
    }
}
