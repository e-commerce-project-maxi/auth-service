package com.example.maxiauthservice.exception;

import com.example.maxiauthservice.enums.ApiExceptionTypeEnum;
import com.example.maxiauthservice.enums.BusinessExceptionEnum;

public class PasswordException extends BaseApiException {
    private static final long serialVersionUID = 540519907941868656L;

    public PasswordException(BusinessExceptionEnum exceptionEnum, Object... params) {
        super(exceptionEnum.getMessage(params),
                exceptionEnum.getCode(),
                exceptionEnum.getReason(params),
                exceptionEnum.getDescription(params));
    }

    public PasswordException(String message, ApiExceptionTypeEnum apiExceptionTypeEnum) {
        super(message, apiExceptionTypeEnum);
    }
}
