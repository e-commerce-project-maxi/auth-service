package com.example.maxiauthservice.exception;


import com.example.maxiauthservice.container.BaseException;
import com.example.maxiauthservice.enums.ApiExceptionTypeEnum;

public class BaseApiException extends RuntimeException implements BaseException {

    private String code;
    private String description;
    private String reason;

    public BaseApiException(String message, String code, String description, String reason) {
        super(message);
        this.code = code;
        this.description = description;
        this.reason = reason;
    }

    public BaseApiException(String message, ApiExceptionTypeEnum exceptionTypeEnum) {
        super(message);
        this.code = exceptionTypeEnum.getCode();
        this.description = exceptionTypeEnum.getDescription();
        this.reason = exceptionTypeEnum.getReason();
    }

    public String getReason() {
        return reason;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
