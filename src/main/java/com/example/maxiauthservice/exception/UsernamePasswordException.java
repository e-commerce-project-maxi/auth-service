package com.example.maxiauthservice.exception;


import com.example.maxiauthservice.enums.ApiExceptionTypeEnum;
import com.example.maxiauthservice.enums.BusinessExceptionEnum;

public class UsernamePasswordException extends BaseApiException{
    private static final long serialVersionUID = -2232928862067245707L;

    public UsernamePasswordException(BusinessExceptionEnum exceptionEnum, Object... params) {
        super(exceptionEnum.getMessage(params),
                exceptionEnum.getCode(),
                exceptionEnum.getReason(params),
                exceptionEnum.getDescription(params));
    }

    public UsernamePasswordException(String message, ApiExceptionTypeEnum apiExceptionTypeEnum) {
        super(message, apiExceptionTypeEnum);
    }
}
