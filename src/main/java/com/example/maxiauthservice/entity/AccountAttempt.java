package com.example.maxiauthservice.entity;

import com.example.maxiauthservice.enums.Status;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class AccountAttempt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    int attemptCount;
    Status status;
    String username;
    Long userId;
    LocalDateTime creationTime;
    LocalDateTime modifiedTime;
}
