package com.example.maxiauthservice.entity.staticData;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public class ParentAuth {

    @Id
    private Short id;
    private String name;
    private boolean activity;
    private LocalDateTime createDate;

    @PrePersist
    public void init(){
        createDate = LocalDateTime.now();
    }
}
