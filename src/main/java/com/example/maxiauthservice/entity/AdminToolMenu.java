package com.example.maxiauthservice.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@Entity
public class AdminToolMenu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "tool_name", nullable = false, length = 50)
    private String toolName;
    private long parentId = 0;
    private boolean isActive;
    private LocalDateTime createdDate;
}
