package com.example.maxiauthservice.entity;

import com.example.maxiauthservice.enums.UserStatus;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
public class BackUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    @Column(unique = true)
    private String username;
    private LocalDateTime createdDate;
    private LocalDateTime updateDate;
    private LocalDateTime passwordExpiredTime;
    private String password;
    private UserStatus status;
    @Column(unique = true)
    private String email;
    boolean accountBlock;
}
