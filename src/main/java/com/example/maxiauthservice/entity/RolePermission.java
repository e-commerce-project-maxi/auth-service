package com.example.maxiauthservice.entity;


import com.example.maxiauthservice.entity.staticData.Permission;
import com.example.maxiauthservice.entity.staticData.Role;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
public class RolePermission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    private Role role;
    @OneToOne
    private Permission permission;
}
